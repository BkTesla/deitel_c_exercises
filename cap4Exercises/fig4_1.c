#include <stdio.h>

/*la funcion main comienza la ejecucion del programa */
int main(){
    int contador = 1; /*inicializacion*/

    while(contador <= 10){
        printf("%d\n", contador);
        ++contador;
    }/* fin del while*/
    return 0;
}/*fin de la funcion main */
