#include <stdio.h>

int main(){
    
    int cuentaCliente;
    int limiteCreditoAnterio;
    int nuevoLimiteCredito;
    int saldoAnterior;
    int credito;

    printf("ingresa el numero de cuenta del cliente\n");
    scanf("%d", &cuentaCliente);

    printf("ingrese el limite de credito anterior\n");
    scanf("%d", &limiteCreditoAnterio);

    nuevoLimiteCredito = limiteCreditoAnterio / 2;

    printf("ingrese el saldo, de adeudo del cliente\n");
    scanf("%d", &saldoAnterior);

    if(saldoAnterior >= nuevoLimiteCredito)
        printf("tu saldo excede tu limite de credito, lo sentimos");
    else{
        credito = nuevoLimiteCredito - saldoAnterior;
        printf("tu nuevo credito es %d\n", nuevoLimiteCredito);
        printf("y el credito disponible es %d\n", credito);
    }
     return 0;
}
