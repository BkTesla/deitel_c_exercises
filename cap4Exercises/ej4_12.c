#include <stdio.h>

int main(){
    
    int numero = 2;
    int suma = 0;

    while(numero <= 30){
        suma += numero;
        numero += 2;
    }
    printf("la suma de los numeros del los pares del 2 al 30 es %d\n", suma);
    return 0;
}
