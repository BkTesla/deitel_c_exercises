#include <stdio.h>
#include <math.h>

int main(){
    
    double suma = 0;
    double hipotenusaCuadrada = 0;

    for(int  ladoA = 1; ladoA <= 500; ladoA++){
        for(int ladoB = 1; ladoB <= 500; ladoB++){
            for(int hipotenusa = 1; hipotenusa <= 10; hipotenusa++){
                suma = pow(double(ladoA), 2) + pow(double(ladoB), 2);
                hipotenusaCuadrada = pow(double(hipotenusa), 2);

                if(hipotenusaCuadrada == suma)
                    printf("la cuadado con lados %d %d y la hipotenosa %d es triple pitagorico \n", ladoA,ladoB, hipotenusa);
            }
        }
    }
    return 0;
}
