#include <stdio.h>

int main(){
    
    int numero = 0;
    int contador = 1;
    int factorial = 1;

    printf("ingrese el numero factorial que quiere calcular\n");
    scanf("%d", &numero);

    for(contador = 1; contador <= numero; contador++){
        
        factorial *= contador;
    }

    printf("el numero factorial de %d es %d\n", factorial, numero);
    return 0;
}
