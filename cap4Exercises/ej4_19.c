#include <stdio.h>

int main(){
    
    int numero = 0;
    float ventas = 0;
    int cantidad;

    while(numero != 9999){
        
        printf("ingrese el codigo del producto\n");
        scanf("%d", &numero);
        if(numero != 9999){
            printf("ingresa la cantidad\n");
            scanf("%d", &cantidad);
            switch(numero){
                case 1:
                    ventas += (2.8 * cantidad);
                    break;
                case 2:
                    ventas += (4.5 * cantidad);
                    break;
                case 3:
                    ventas += (9.98 * cantidad);
                    break;
                case 4:
                    ventas += (4.49 * cantidad);
                    break;
                case 5:
                    ventas += (6.87 * cantidad);
                    break;
                default:
                    printf("codigo no exite\n");
                    break;
            }
        }
    }
    printf("Las ventas de la semana fueron %d\n", ventas);
    return 0;
}
