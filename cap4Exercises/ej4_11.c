#include <stdio.h>

int main(){
    
    int cantidad;
    int menor = 0;
    int numero;
    int contador = 1;

    printf("ingresa cuantos numeros quieres ingresar\n");
    scanf("%d", &cantidad);

    while(contador <= cantidad){
        printf("ingresa el numero \n");
        scanf("%d", &numero);
        if(menor == 0)
            menor = numero;
        if(numero < menor)
            menor = numero;
        contador++;
    }

    printf("el numero menor ingresado es %d\n", menor);
    return 0;
}
