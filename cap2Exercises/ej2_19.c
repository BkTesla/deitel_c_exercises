#include <stdio.h>

// this program take 3 numbers from the user and calculate the minor, the largest
// sum and the average of the three numbers 

// in this chapter there are still no functions used to solve the exercises.
int main(){
    //define variables
    int number1;
    int number2;
    int number3;
    int bigNumber;
    int smallNumber;
    int promedio;
    int suma;
    int producto;

    //get number1
    printf("ingresa el primer numero\n");
    scanf("%d", &number1);

    //get number2
    printf("ingrese el segundo numero\n");
    scanf("%d", &number2);

    //get number3
    printf("ingrese el trcer numero\n");
    scanf("%d", &number3);
    
    //calculate add
    suma = number1 + number2 + number3;
    printf("la suma de los numeros es %d\n", suma);

    //calculate product
    producto = number1 * number2 * number3;
    printf("el producto de lo numero es %d\n", producto);

    //calculate average
    promedio = suma/3;
    printf("el promedio de los tres numeros es %d\n", promedio);

    //get smaller number
    smallNumber = number1;
    if(smallNumber > number2)
        smallNumber = number2;
    if(smallNumber > number3)
        smallNumber = number3;
    printf("el numero mas pequeno es %d\n", smallNumber);

    //get bigger number
    bigNumber = number1;
    if(bigNumber < number2)
        bigNumber = number2;
    if(bigNumber < number3)
        bigNumber = number3;
    printf("numero mayor es %d\n", bigNumber);
    return 0;
}
