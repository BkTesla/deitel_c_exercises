#include <stdio.h>

int main(){
    int contador = 1;

    printf("N\t10*N\t100*N\t1000*N\t\n");
    while(contador <= 10){
        printf("%d\t%d\t%d\t%d\t\n", contador, contador * 10, contador * 100, contador * 1000);
        contador++;
    }
    return 0;
}
