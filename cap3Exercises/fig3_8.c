#include <stdio.h>

int main(){
    /*programa para obtener el promedio de calificaciones de un grupo mediante la repeticion controlada por
    centinela*/

    int contador = 0;
    int calificacion;
    int total = 0;

    float promedio;
    
    printf("introduzca la calificacion -1 para terminar: \n");
    scanf("%d", &calificacion);

    while ( calificacion != -1){
        total = total  + calificacion;
        contador++;
        printf ("introduzca la calificacion -1 para terminar: \n");
        scanf("%d", &calificacion);
    }
    if (contador != 0){
        promedio = ( float) total / contador;
        printf("el promedio del grupo es %.2f\n", promedio);
    }
    else{
        printf(" no se introdujeron calificaciones\n");
    }

    return 0;
}
