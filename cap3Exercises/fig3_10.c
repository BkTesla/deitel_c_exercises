#include <stdio.h>

int main(){
    int aprobados = 0;
    int reprobados = 0;
    int estudiantes = 1;
    int resultado;

    while( estudiantes <= 10){
        printf("intoduzca el resultado (1=aprobado 2=reprobado): ");
        scanf("%d", &resultado);

        if(resultado == 1)
            aprobados++;
        else
            reprobados++;
        estudiantes++;
    }   
    printf("Aprobados %d\n", aprobados);
    printf("Reprobados%d\n", reprobados);

    if(aprobados >= 8)
        printf("objetivo alcanzado");

    return 0;
}
