#include <stdio.h>

int main(){
    int numero = 1;
    int suma = 0;
    
    while(numero < 11){
        suma += numero;
        numero++;
    }
    printf("%d\n", suma);
    return 0;
}
