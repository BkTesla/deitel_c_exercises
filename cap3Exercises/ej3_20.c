#include <stdio.h>

int main(){
    float prestamo = 0;
    float tasaInteres = 0;
    float periodoTiempo = 0;
    float montoInteres = 0;

    while(prestamo != -1){
        printf("ingresa el monto del prestamo\n");
        scanf("%f", &prestamo);

        if(prestamo != -1){
            printf("introduzca el periodo de tiempo\n");
            scanf("%f", &periodoTiempo);

            printf("introduzca el la taza de intere\n");
            scanf("%f", &tasaInteres);

            montoInteres = prestamo * tasaInteres * periodoTiempo / 365;
            printf("el monto del interes es %.2f\n", montoInteres);
        }
    }
    return 0;
}
