#include <stdio.h>

int main(){
    
    int horasLabordas = 0;
    float pago = 0;
    int horasNormales = 0;
    int horasExtras = 0;
    float pagoTotal = 0;;
    float pagoNormal = 0;
    float pagoExtra = 0;

    while(horasLabordas != -1){
        printf("ingrese el numero de horas laboradas por el empleado (-1 para terminar): \n");
        scanf("%d", &horasLabordas);

        if(horasLabordas != -1){
            printf("ingrese el pago por hora del empleado\n");
            scanf("%f", &pago);

            if(horasLabordas > 40){
                horasNormales = 40;
                horasExtras = horasLabordas - 40;
                pagoNormal = horasNormales * pago;
                pagoExtra = (horasExtras * pago) * 1.5;
                pagoTotal = pagoNormal + pagoExtra;
                printf("el pago total del empleado es %.2f\n", pagoTotal);
            }
            else
                printf("el pago total del empleado es %.2f\n", horasLabordas * pago);
        }
    }
    return 0;
}
