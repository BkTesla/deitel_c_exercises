#include <stdio.h>

int main(){
    
    int size = 0;
    int i = 0;

    printf("ingresa el tamano del cuadrado\n");
    scanf("%d", &size);

    for(i = 1; i <= size; i++){
        for(int j = 1; j <= size;j++){
            if( i == 1 || i == size)
                printf("*");
            else{
                if(j == 1 || j == size)
                    printf("*");
                else
                    printf(" ");
            }
        }
        printf("\n");
    }
    return 0;

}
