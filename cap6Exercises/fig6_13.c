#include <stdio.h>
#define TAMANIO 5

/*Prototipo de las funciones*/
void modificarArreglo(int b[], int tamanio);
void modificarElemento(int e);

int main(void){
 int a[TAMANIO] =  {0,1,2,3,4} ;
 int i;

 printf("Efectos de pasar arreglos compleros por referencia :\n\n los ""valores del arreglo original son \n");
 for(i = 0; i < TAMANIO; i++){
  printf("%3d", a[i]);
 }

 printf("\n");


  printf("los valores a modificar por referencia\n");
  modificarArreglo(a, TAMANIO);

  printf("los valores del arreglo son\n");
  for(i = 0; i < TAMANIO; i++){
    printf("%3d", a[i]);
 }

 printf("\n\n efectos de pasar los elementos del arreglo "
        "Por valor :\nEl valor de a[3] es %d\n", a[3]);

  modificarElemento(a[3]);
  printf("El valor de a[3] es %d\n", a[3]);
  return 0 ;
}

void modificarArreglo(int b[], int tamanio)
{
  int j;
  for(j = 0;j < tamanio; j++)
  {
    b[j] *= 2;
  }
} 


void modificarElemento(int e){
  printf("El valor en modifica elemnto es %d\n", e *= 2);
}
