#include <stdio.h>

void IniciaArregloEstatico(void);
void IniciaArregloAutomatico(void);

int main(void){

  printf("la primera llamada a la funcion\n");
  IniciaArregloAutomatico();
  IniciaArregloEstatico();

  printf("\n\nSegunda llamada a la cada funcion");
  IniciaArregloEstatico();
  IniciaArregloAutomatico();

  return 0;
}


void IniciaArregloEstatico(void){

  static int arreglo1[3];
  int i;

  printf("\n valores al entrar al inicio del arreglo Estatico:\n");

  for(i = 0; i <= 2; i++){
    printf("arreglo1[%d] = %d ", i, arreglo1[i]);
  }
  
  printf("\n Valores al salir del arreglo\n");

  for(i = 0; i <=2; i++){
    printf("\narreglo1[%d] = %d", i, arreglo1[i] += 5);
  }
}


void IniciaArregloAutomatico(){
  int arreglo2[3] = {1,2,3};
  int i;

  printf("\n\nValores al entrar a iniciar arreglo Automatico:\n");

  for(i = 0; i <= 2; i++){
    printf("arreglo2[%d] = %d ", i, arreglo2[i]);
  }

  printf("\nValores al salir de IniciaArregloAutomatico:\n");
  for(i = 0; i <= 2; i++){
    printf("arreglo2[%d] = [%d] \n", i, arreglo2[i] += 5);
  }
}
