// A cuarenta estudiantes se les pregunto al respecto a la calidad de la comida
//  de la cafeteria, en una escala del uno al 10
//  1 significa muy mala y 10 exelente.
//  coloque las 40 respuestas en un arreglo entero que resuma l9os resultados de la encuesta
//

#include <stdio.h>
#define TAMANIO 12

int main(void){
    int a[TAMANIO] = {1, 3, 4,5, 7, 2, 99, 16, 45, 67, 89, 45};
    int i;
    int total = 0;

    for(i=0; i < TAMANIO; i++){
      total += a[i];
    }
    printf("el total de los elementos del arreglo es %d\n", total);
    return 0;
}
