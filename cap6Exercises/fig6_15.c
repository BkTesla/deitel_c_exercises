#include <stdio.h>
#define TAMANIO 10

int main(void){
  
  int a[TAMANIO] = {2,6,4,8, 10, 12,89, 68, 45, 37};
  int pasadas;
  int i; 
  int almacena;

  printf("Elementos de datos en el orden de origen\n");

  for(i = 0; i < TAMANIO; i++)
    printf("%4d", a[i]);
  
  printf("\n");

  /* ordenamiento de Burbuja*/
  for(pasadas = 1; pasadas < TAMANIO; pasadas++){
    /* Compara los elementos adyacentes y los intercambia si el primer
    elemento es mayor que el segundo*/
    for(i = 0; i < TAMANIO; i++){
      if(a[i] > a[i + 1]){
        almacena = a[i];
        a[i] = a[i +1];
        a[i + 1] = almacena;
      }
    }
  }
  printf("los elementos ordenados\n");

  for(i = 0; i < TAMANIO; i++){
    printf("%4d", a[i]); 
  }
  printf("\n");
 return 0;
}
