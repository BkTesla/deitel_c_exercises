#include<stdio.h>

void modificarArreglo(const int b[]);

int main(void){
  int a[] = {10, 20,30};
  modificarArreglo(a);
  printf("%d %d %d", a[0], a[1], a[2]);
  return 0;
}

void modificarArreglo(const int b[])
{
  b[0] /= 2;
  b[1] /= 2;
  b[2] /= 2;
}
