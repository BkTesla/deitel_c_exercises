#include <stdio.h>

int main(void){
  char cadena1[20];
  char cadena2[] = "literal de cadena"; /*reserva 18 caracteres*/
  int i;

  printf("introduce una cadena: ");
  scanf("%s", cadena1); 


  printf("La cadena1 es: %s\ncadena2 es: %s\n"
          "la cadena1 con espacios entre caracteres es:\n",
          cadena1, cadena2);

  for (i = 0; i< cadena1[i] != '\0'; i++){
    printf("%c ", cadena1[i]);
  }

  printf("\n");
  return 0;
}
