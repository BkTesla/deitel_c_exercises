#include <stdio.h>

int main(void){
    
    for(int num = 1; num <= 10000; num++){
        int suma = 0; 
        for(int i = 1; i <= num; i++){
            if(0 == num % i){
                suma++;
            }
        }
        if(2 == suma){
            printf("el numero %d es primo\n", num);
        }
    }

    return 0;
}
