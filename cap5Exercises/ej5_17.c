//exercise 5.17.c
//this program calculate if the number it's multiple of the base
#include <stdio.h>

void  multiple(int base, int multiple);

int main(void){
    int getbase;
    int getmultiple;
    printf("este programa calculara si un numero es multiplo de la base ingresada\n");
    printf("ingrese la base \n");
    scanf("%d", &getbase);

    printf("ingrese el numero para saber si es multiplo de la base \n");
    scanf("%d", &getmultiple);

   multiple(getbase, getmultiple);

   return 0;
}

//function multiple recive two int numbers base and multiple, calculate and printf if es multiple of base or not
void  multiple(int base, int multiple){
    if(multiple % base == 0)
        printf("%d es multiplo de %d\n", multiple, base);
    else
        printf("%d no es multiplo de %d\n", multiple, base);
}
