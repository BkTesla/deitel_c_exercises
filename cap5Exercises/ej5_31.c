#include <stdio.h>
#include <stdlib.h>
#include <time.h>

int headsOrTails(void);

int main(void){
    int resultado; 
    int tails = 0;
    int head = 0;
    
    printf("este programa simula el lanzamiento de una moneda 100 veces\n");

    for(int i = 1; i <= 100; i++){
       resultado = headsOrTails(); 
       
        if(resultado == 1)
            head++;
        else
            tails++;
    }

    printf("CARAS ------> %d\n", head);
    printf("CRUZ  ------> %d\n", tails);
}

int headsOrTails(void){
    srand(time(NULL));

    return (rand() % 2 + 1);
}
