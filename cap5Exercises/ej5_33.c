#include <stdio.h>
#include <stdlib.h>
#include <time.h>

int main(void){
    
    int number1;
    int number2;
    int total;
    int answer;
    int message;

    srand(time(NULL));

    printf("Este programa te arrojara dos numeros aletorios y preguntara al usuario cual es el resultado del producto de los numeros\n");
    
    number1 = (rand() % 9 + 1);
    number2 = (rand() % 9 + 1); 
    total = number1 * number2;

    do{
        printf("ingresa el resultado de multiplicar %d * %d\n", number1, number2);
        scanf("%d", &answer);

        message = rand() % 4 + 1;
        if(total == answer){
            switch(message){
                case 1:
                    printf("MUY BIEN!!\n");
                    break;
                case 2:
                    printf("BUEN TRABAJO!\n");
                    break;
                case 3:
                    printf("EXELENTE\n");
                    break;
                case 4:
                    printf("MANTEN ESE BUEN RENDIMIENTO\n");
                    break;
            }
        }

        else{
            switch(message){
                case 1:
                    printf("NO POR FAVOR INTENTA DE NUEVO\n");
                    break;
                case 2:
                    printf("INCORRECTO TRATA UNA VEZ MAS\n");
                    break;
                case 3:
                    printf("NO TE RINDAS\n");
                    break;
                case 4:
                    printf("NO, SIGUE INTENTANDO\n");
                    break;
            }
        }
    }while(total != answer);

    return 0;
}

