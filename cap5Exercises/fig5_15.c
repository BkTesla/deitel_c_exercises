/*figura 5.15
 * funcion recursiva de fibonacci*/

#include <stdio.h>

long fibonacci( long n);

int main(void){
  long resultado;
  long numero;
  
  printf("introduzca un numero entero\n");
  scanf("%ld", &numero);

  resultado  = fibonacci( numero );

  printf("Fibonacci ( %ld ) = %ld\n", numero, resultado);

  return 0;
}

/*definicion de la funcion recursiva */

long fibonacci(long n){
  
  if (n == 0 || n== 1){
    return n;
  }
  else{
      return fibonacci(n - 1) + fibonacci( n - 2);
  }
}
