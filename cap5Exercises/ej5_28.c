#include <stdio.h>

int reverseNumber(int number);

int main(void){
    
    int number = 0;
    int reverse = 0;

    printf("ingrese el numero que quiera invertir\n");
    scanf("%d", &number);
    reverse = reverseNumber(number);

    printf("el numero invertido es %d", reverse);

    return 0;
}

int reverseNumber(int number){
    int reverse = 0;
    int reminder = 0;

    while(number != 0){
        
        reminder = number % 10;
        reverse = reverse * 10 + reminder;
        number /= 10;
        printf("reverse es = a %d\n", reverse);
    }

    return reverse;
}
