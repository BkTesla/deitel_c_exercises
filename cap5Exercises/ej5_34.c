#include <stdio.h>
#include <stdlib.h>
#include <time.h>

void rightPercent(int right, int fail);

int main(void){
    
    int number1, number2, total, answer, message;
    
    int fail = 0;
    int right = 0;

    srand(time(NULL));
    printf("Este programa te arrojara dos numeros aletorios y preguntara al usuario cual es el resultado del producto de los numeros\n");

    for(int i = 1; i <= 10; i++){
        
        number1 = (rand() % 9 + 1);
        number2 = (rand() % 9 + 1); 
        total = number1 * number2;

        do{
            printf("ingresa el resultado de multiplicar %d * %d\n", number1, number2);
            scanf("%d", &answer);

            message = rand() % 4 + 1;
            if(total == answer){
                switch(message){
                    case 1:
                        printf("MUY BIEN!!\n");
                        break;
                    case 2:
                        printf("BUEN TRABAJO!\n");
                        break;
                    case 3:
                        printf("EXELENTE\n");
                        break;
                    case 4:
                        printf("MANTEN ESE BUEN RENDIMIENTO\n");
                        break;
                }

                right++;
            }

            else{
                switch(message){
                    case 1:
                        printf("NO POR FAVOR INTENTA DE NUEVO\n");
                        break;
                    case 2:
                        printf("INCORRECTO TRATA UNA VEZ MAS\n");
                        break;
                    case 3:
                        printf("NO TE RINDAS\n");
                        break;
                    case 4:
                        printf("NO, SIGUE INTENTANDO\n");
                        break;
                }

                fail++;
            }
        }while(total != answer);
    }
    
    return 0;
}

void rightPercent(int right, int fail){
    
    int totalTry = right + fail;
    float percent = (right * 100) / totalTry;

    if(percent < 75)
        printf("PIDE AYUDA A TU PROFESOR PARA QUE TE ORIENTE\n\n\n");

    return;
}

