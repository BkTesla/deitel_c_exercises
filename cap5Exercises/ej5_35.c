#include <stdio.h>
#include <stdlib.h>
#include <time.h>

int main(void){
    
    int number1;
    char nextTry;
    int getNumber = 0;

    printf("Tengo un numero entre 1 y 100\n");
    printf("puedes adivinarlo?\n");

    srand(time(NULL));

    do{
        number1 = 1 + rand() % 100;
        while(getNumber != number1){
            
            printf("ingresa tu respuesta\n");
            scanf("%d", &getNumber);

            if(getNumber == number1)
                printf("EXELENTE ADIVINASTE!\n");

            else if(getNumber > number1)
                printf("Muy alto \n");

            else
                printf("Muy bajo\n");
        }

        printf("Quieres jugar otra vez?, oprime cualquier letra (exepto 'n')  para volverlo a intentarlo o 'n' para terminar\n");
        scanf(" %c", &nextTry);
        
    }while(nextTry != 'n' && nextTry != 'N');
    
    return 0;
}
