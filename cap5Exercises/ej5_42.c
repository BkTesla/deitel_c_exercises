#include <stdio.h>

int mcd(int num1, int num2);

int main(void){
    
    int number1;
    int number2;
    int maxDiv;

    printf("este programa lee dos numero y calcula el maximo comun divisor\n");
    printf("\n ingrese el primer numero\n");
    scanf("%d", &number1);
    printf("ingrese el segundo numero\n");
    scanf("%d", &number2);

   maxDiv = mcd(number1, number2);

   printf("El maximo comun divisor de %d y %d es %d\n", number1, number2, maxDiv);

   return 0;
}

int mcd(int num1, int num2){
    
    int maxDiv = 1;  
    for(int i = 2; i <= num1 && i <= num2; i++){
        if(num1 % i == 0 && num2 % i == 0)
            maxDiv = i;
    }

    return maxDiv;
}
