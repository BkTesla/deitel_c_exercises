//this program print square make with #
#include <stdio.h>

void squareHash(int sizeSide);

int main(){
    
    int side;
    printf("este programa imprimira un cuadrado echo con signos #\n");
    printf("ingrese el numero de tamano del cuadrado que quiere imprimir\n");
    scanf("%d", &side);

    squareHash(side);
    
    return 0;
}

//squareHash function get a number and printf a square with base x height #
void squareHash(int sizeSide){
    for(int i = 1; i <= sizeSide; i++){
        for(int j =1; j <= sizeSide; j++)
            printf("#");
        printf("\n");
    }
}
