#include <stdio.h>

int calculateSecond(int hours, int minutes,  int seconds);

int main(void){
    
    int hours;
    int minutes;
    int seconds;
    int totalSeconds;

    printf("este programa regresa el tiempo transcurrido entre las 12 y la hora indicada\n");
    printf("solo calcula periodos de 12 horas y no distingue entre AM y PM\n");
    do{
        printf("ingrese la hora\n");
        scanf("%d", &hours);
        if(hours >= 12 || hours < 0)
            printf("ingrese un numero menor a 12\n");
    }while(hours >= 12 || hours < 0);
    
    do{
        printf("ingrese los minutos\n");
        scanf("%d", &minutes);
        if(minutes >= 59 || minutes < 0)
            printf("ingrese un numero menor a 59\n");
    }while(minutes > 59 || minutes < 0);

    do{
        printf("ingrese los segundos\n");
        scanf("%d", &seconds);
         
        if(seconds >= 59 || seconds < 0)
            printf("ingrese un numero menor a 59\n");
    }while(seconds > 59 || seconds < 0);

    totalSeconds = calculateSecond(hours, minutes, seconds);

    printf("la cantidad de segundos transcurridos desde las 12:00:00 hasta las %d:%d:%d son %d\n", hours, minutes, seconds, totalSeconds);
}

int calculateSecond(int hours, int minutes, int seconds){

    int totalSeconds = 0;
    return totalSeconds = seconds + (minutes * 60) + ((hours * 60) *60);
}
