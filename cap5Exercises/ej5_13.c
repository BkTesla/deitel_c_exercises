//this program generates random number in a range

#include <stdio.h>
#include <time.h>
#include <stdlib.h>

int main(){
    
    printf("este programa muestra varios numeros aleatorios en diferentes rangos\n");

    srand(time(NULL));
    printf("%d\n",(rand() % 2 +1));

    printf("%d\n", (rand() % 100 + 1));
    printf("%d\n",(rand() % 9));
    printf("%d\n",(rand() % (1112 - 1000 + 1)) + 1000);
    printf("%d\n",(rand() % (1 -1 + 1)));
    printf("%d\n",(rand() % 11 -3));

    return 0;
}
