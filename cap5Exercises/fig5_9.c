//Randomizascion del programa de dados
//
#include <stdlib.h>
#include <stdio.h>


int main(void){
    
  int i;
  unsigned semilla;

  printf("introduzca la semilla: ");
  scanf("%u", &semilla);

  srand(semilla);
  
  for(i = 1; i <= 10; i++){
    printf("%10d", 1 + (rand() % 6));

    if(i % 5 == 0)
      printf("\n");
  }

  return 0;
}
