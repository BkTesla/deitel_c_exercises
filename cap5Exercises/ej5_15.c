//exercise 5.15 get base and height of a triangle and call function that calculate the size that hipotenuse 

#include <stdio.h>
#include <math.h>

double hipotenusa(double a, double b);
int main(){
    
    double base;
    double altura;

    printf("Este programa calcula la hipotenusa de un triangulo\n");
    printf("Ingresa la base del triangulo\n");
    scanf("%lf", &base); // get base
    printf("ingresa la altura del triangulo\n");
    scanf("%lf", &altura);// get height

    printf("la hipotenusa del triangulo es %.2lf\n", hipotenusa(base, altura));
    return 0;
}

//function calculte hipotenusa
double hipotenusa(double a, double b){
    
    return sqrt(pow(a,2) + pow(b,2)); 
}

