#include <stdio.h>

int potencia(int base, int exponente);

int main(){

    int base;
    int exponente;
    int result;
    printf("ingrese el numero del cual quieres calcular la potencia\n");
    scanf("%d", &base);
    
    printf("ingresa la potencia a la cual quieres calcular la base\n");
    scanf("%d", &exponente);
    
    result = potencia(base, exponente);
    printf("la potencia de %d a la %d es %d\n", base, exponente, result);
    
    return 0;
}

int potencia(int base, int exponente){
    
    if(exponente == 1)
        return base;
    else
        return base * potencia(base, exponente -1);
}
