//this program show how function floor() to round number
//The C library function double floor(double x) returns the largest integer value less than or equal to x
#include <stdio.h>
#include <math.h>

int main(){
    
    int totalNumbers = 0;
    float  number;

    printf("ingresa cuantos numeros quieres redondear\n");
    scanf("%d", &totalNumbers);

    for(int i = 1; i <= totalNumbers; i++){
        printf("ingresa un numero\n");
        scanf("%f", &number);

        printf("el numero ingresado es %f\n", number);
        printf("el valor redondeado es %f\n", floor(number));
    }

    return 0;
}
