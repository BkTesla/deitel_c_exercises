#include <stdio.h>
#include <stdlib.h>
#include <time.h>

int main(void){
    
    int number1;
    int number2;
    int total;
    int answer;

    srand(time(NULL));

    printf("Este programa te arrojara dos numeros aletorios y preguntara al usuario cual es el resultado del producto de los numeros\n");
    
    number1 = (rand() % 9 + 1);
    number2 = (rand() % 9 + 1); 
    total = number1 * number2;

    do{
        printf("ingresa el resultado de multiplicar %d * %d\n", number1, number2);
        scanf("%d", &answer);

        if(total == answer)
            printf("CORRECTO!\n");
        else
            printf("intente de nuevo porfavor\n");
    }while(total != answer);

    return 0;
}

