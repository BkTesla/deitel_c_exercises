#include <stdio.h>

//this program calculate the  greatest common divisor use two function 
//the first use euclidian algorithm
//and the second use for loop for.

int MCD(int number1, int number2);

int option2MCD(int number1, int number2);

int main(void){
    
    int getNumber1;
    int getNumber2;
    int commonDivisor = 0;

    printf("ingresa el primer numero\n");
    scanf("%d", &getNumber1);

    printf("ingresa el segundo numero\n");
    scanf("%d", &getNumber2);
    
    commonDivisor = MCD(getNumber1, getNumber2);

    printf("el maximo  comun divisor de %d y %d es %d\n", getNumber1, getNumber2, commonDivisor);

    commonDivisor = option2MCD(getNumber1, getNumber2);
    printf("el maximo comun divisor es %d\n", commonDivisor);

    return 0;
}


int MCD(int number1, int number2){
    
    int higher = number1;
    int less = number2; 
    int quotient, remainder, mcd;
    
    
    if(number2 > higher){
        higher = number2;
        less = number1;
    }

    do{
        
        quotient = higher / less;
        remainder = higher % less;

        if(remainder != 0){
            
            mcd = remainder;
            higher = less;
            less = remainder;

        }

    }while(0 != remainder);

    return mcd;
}

int option2MCD(int number1 , int number2){
    int i;
    int mcd;
    for(i = 1; i <= number1 && i <= number2;i++){
        
        if(0 == number1 % i && 0 == number2 % i)
            mcd = i;
    }
    return mcd;
}
