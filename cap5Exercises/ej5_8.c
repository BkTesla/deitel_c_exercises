//this program calculate the price to pay in a parking space according to the time
//time < 3  hours pay 2.00 dlls
//time > a 3 hours  pay .5 dlls for additional hour or fraction 
//24 hours period pay 10 dlls 

#include <stdio.h>
#include <math.h>//include math library

float cobrarHoras(float numeroHoras);

int main(){
    float  horasEstacionado; 
    float cobroTotal = 0;

    //get park period time
    printf("ingresa el numero de horas que estuvo estacionado el auto\n");
    scanf("%f", &horasEstacionado);

    //call cobrarHoras Function and assing value to float variable 
    cobroTotal = cobrarHoras(horasEstacionado);
    printf(" el cobro total de las horas es %.2f\n", cobroTotal);
    return 0;
}


float cobrarHoras(float numeroHoras){
    
    float fraccionHoras = 0;
    int horasCompletas = 0;
    float cobro = 0;

    if(numeroHoras <= 3)
        return 2.00;
    if(numeroHoras > 3){
        printf("hola condicion");
        horasCompletas = numeroHoras / 1;
        
        //use fmod to get the residue or fraction hours
        fraccionHoras = fmod(numeroHoras, 1.0);
        horasCompletas = horasCompletas - 3;
        cobro = (horasCompletas * 0.5) + 2.00;
        if(fraccionHoras > 0)
            cobro = cobro + 0.5;

        //this program assumes that the time period is not longer that  24 hours
        if(cobro > 10.00)
            cobro = 10;
        return cobro;

    }
}
