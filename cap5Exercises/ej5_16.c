//exercice 5.16
//this program get two number base and exponent and calculate result
#include <stdio.h>

int potencia(int base, int exponente);

int main(){
    
    int base;
    int exponente;

    printf("Ingresa el numero que del cual quieres calcular su exponente \n");
    scanf("%d", &base); //get base
    printf("ingresa el exponente \n");
    scanf("%d", &exponente); //get exponent 

    printf("la potencia del numero %d al exponente %d es %d\n", base, exponente, potencia(base, exponente));
    return 0;
}

//function potencia  return base^exponente
int potencia(int base, int exponente){
    int suma = 1;
    for(int i = 1; i <= exponente; i++)
        suma *= base; 
    return suma;
}
