//exercise 5.18
//this program calculate if the number entered is odd even 
#include <stdio.h>

int impar(int number);

int main(void){
    int getNumber;

    do{
        printf("ingrese un numero y el programa le dira si es par o impar (9999 para terminar)\n");
        scanf("%d", &getNumber);
        if(getNumber != 9999){
            if(impar(getNumber))
                printf("el numero %d es impar\n", getNumber);
            else
                printf("el numero %d es par\n", getNumber);
        }
    }while(getNumber != 9999);
    
    return 0;
}

//calculte and reurn if number is even or odd 
int impar(int number){
    return (number % 2 ==0);
}
