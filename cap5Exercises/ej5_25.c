#include <stdio.h>

float lesserFloat(float one, float two, float three);
int main(void){
    
   float one, two, three, lesser;
   printf("Este programa lee tres numeros y regresa el mas pequeno\n");

   printf("ingresa el primer numero\n");
   scanf("%f", &one);

   printf("ingresa el segundo numero\n");
   scanf("%f", &two);

   printf("ingresa el tercer numero\n");
   scanf("%f", &three);

   printf("el menor de los numeros entre %.2f, %.2f, %.2f es %.2f \n", one, two , three, lesserFloat(one, two, three));

   return 0;
}

float lesserFloat(float one, float two, float three){
    
    float lesser  = one;
    if(two < lesser)
        lesser = two;
    if(three < lesser)
        lesser = three;

    return lesser;
}
