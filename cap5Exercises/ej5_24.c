#include <stdio.h>

float celsiusToFarenheit(float celsius);
float farenheitToCelsius(float farenheit);
int main(){
    
    float farenheitDegrees, CelsiusDegrees;

    printf("grados celsius\tgrados Farenheit\n");
    for(int i = 1; i <= 100; i++){
       farenheitDegrees = celsiusToFarenheit(i);
       printf("%d\t%13.2f\n", i, farenheitDegrees);
    }

    printf("\n\ngrados Farenheit\tGrados Celsius\n");

    for(int i = 32; i <= 212; i++){
        CelsiusDegrees = farenheitToCelsius(i);
        printf("%d\t%15.2f\n", i, CelsiusDegrees);
    }

    return 0;
}


float celsiusToFarenheit(float  celsius){
    
    float convertion;
    return convertion = celsius * 9 / 5 + 32;
}

float farenheitToCelsius(float farenheit){
    
    float convertion;
     return convertion = (farenheit - 32) * 5 /9;
}
