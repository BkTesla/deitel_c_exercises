//exercise 5.22
//this program calculate quotient and residue of a division 
#include <stdio.h>

int getQuotient(int dividend, int divider);
int getResidue(int dividend, int divider);
void splitNumbers(int number);

int main(void){
    
    int getDividend;
    int getDivier;
    int splitNumber;

    printf("Este programa calcula el cociente y el residuo de una division\n");
    printf("entre a / b\n\n");

    printf("ingresa el dividendo\n");
    scanf("%d", &getDividend);

    printf("ingresa el divisor\n");
    scanf("%d", &getDivier);

    printf("el cociente de %d dividio entre %d es %d\n", getDividend, getDivier, getQuotient(getDividend, getDivier));

    printf("el residuo de %d dividido entre %d es %d\n", getDividend, getDivier, getResidue(getDividend, getDivier));

    do{
        printf("ingresa el numero que quieres separar en digitos individuales\n");
        printf("recuerda que tiene que se un numero mayor a 10000 y 32767\n");
        scanf("%d", &splitNumber);
        if(splitNumber < 10000 || splitNumber > 32767)
            printf("\ningresa un numero valido\n");
        else
            splitNumbers(splitNumber);
    }while(splitNumber < 10000 ||  splitNumber > 32767);

    return 0;
}

int getQuotient(int dividend, int divider){
    
    return dividend / divider;
}

int getResidue(int dividend, int divider){
    
    return dividend % divider;
}

void splitNumbers(int number){
    
    int numberUnit;
    int numberTen;
    int numberHundreds;
    int numberThousand;
    int numberTenThousand;
    int pivot;
    if(number < 1 || number > 32767){
        printf("el numero ingresado no esta dentro del rango\n");
        return;
    }

    numberUnit = getResidue(number,10);
    pivot = getQuotient(number, 10);
    numberTen = getResidue(pivot, 10);
    pivot  = getQuotient(pivot, 10);
    numberHundreds = getResidue(pivot, 10);
    pivot = getQuotient(pivot,10);
    numberThousand = getResidue(pivot, 10);
    numberTenThousand = getQuotient(pivot, 10);
    
    printf("%d\t%d\t%d\t%d\t%d\n",numberTenThousand, numberThousand, numberHundreds, numberTen, numberUnit);
    return;
}
