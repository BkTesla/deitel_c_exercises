#include <stdio.h>

double fib(double num1, double num2);

int main(void){
    
    double number1 = 0;
    double number2 = 1;
    double fibonaci = 0; 
    int b = 1;

    printf("este programa imprime la serie de fibonaci\n");
    printf("%.0lf %.0lf ", number1, number2);

    while(b < 100){
       fibonaci = fib(number1, number2); 
       printf("%.0lf ", fibonaci);
       number1 = number2;
       number2 = fibonaci;
       b++;
    }

    return 0;
}

double fib(double num1, double num2){

    return num1 + num2;
}


