#include <stdio.h>

int qualityPoints(int avarege);

int main(void){

    //this program get  a student grade and return integer number in relation to this
    int grade = 0;
    int points;
    do{
        printf("ingrese la calificacion del estudiante para calcular el valor para la calificacion final del trabajo\n");
        scanf("%d", &grade);

        if(grade > 100)
            printf("el rango de calificaciones es de 0-100, FAVOR de ingresar un numero en ese rango");

    }while(grade > 100);

    points = qualityPoints(grade);
    printf(" los puntos acumulativos para la calificacion final son %d", points);
    
    return 0;
}

int qualityPoints(int avarege){
    
    if(avarege >= 90 && avarege <=100)
        return 4;
    else if(avarege >= 80 && avarege <= 89)
        return 3;
    else if(avarege >= 70 && avarege <= 79)
        return 2;
    else if(avarege >= 60 && avarege <= 69)
        return 1;
    else 
        return 0;
}
